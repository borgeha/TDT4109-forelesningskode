'''
Koden her ligger på utsiden av pensum, og er kun men å vise styrken til
python nå en på toppen av det dere lærer kan legge ekstra pakker (moduler)
'''

from geopy.geocoders import Nominatim
from geopy import distance
from geopy.distance import geodesic 
geolocator = Nominatim(user_agent="foo")
location1 = geolocator.geocode(input("Sted 1: "))
location2 = geolocator.geocode(input("Sted 2: "))

print(f'Regner ut avstanden mellom {location1} og {location2}...')

km = geodesic((location1.latitude, location1.longitude), (location2.latitude, location2.longitude)).km


print(f"Avstanden mellom {location1} og {location2} er {km:.2f} km.")
